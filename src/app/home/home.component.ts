import { Component, OnInit } from '@angular/core';
import { SharedService } from '../Servise/shared.service';
import { HttpClient } from '@angular/common/http';
import { Insurance } from '../module/insurance';

declare let $:any; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  url: string = "";
  ageList = ["20-25","26-30","31-35","36-40","41-45","46-50","51-55"];
  incomeList = ["3-5","5-7","7-10","10+"];
  gotResponse: boolean = false;
  loader: boolean = false;
  errMessage: string = "";
  showInsurance: boolean = false;
  constructor(public sharedService: SharedService, private http: HttpClient) { }

  ngOnInit() {
  }

  onSelectFile(event) { // called each time file input changes
    this.loader = true;
    this.url = "";
    this.errMessage = "";
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      //this.fileChange(event);
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
        
        this.http.post<any>("http://localhost:8000/getDat",{base64: this.url})
          .subscribe(
            res => {
              console.log(JSON.parse(res));
              let data = JSON.parse(res);
              if(data.faces){
                let age = data.faces[0].attributes.age.value;
                if(age >= 20 && age <=25){
                  this.sharedService.selfieResponse.age = "20-25"
                }
                else if(age >= 26 && age <=30){
                  this.sharedService.selfieResponse.age = "26-30"
                }
                else if(age >= 31 && age <=35){
                  this.sharedService.selfieResponse.age = "31-35"
                }
                else if(age >= 36 && age <=40){
                  this.sharedService.selfieResponse.age = "36-40"
                }
                else if(age >= 41 && age <=45){
                  this.sharedService.selfieResponse.age = "41-45"
                }
                else if(age >= 46 && age <=50){
                  this.sharedService.selfieResponse.age = "46-50"
                }
                else if(age >= 51 && age <=55){
                  this.sharedService.selfieResponse.age = "51-55"
                }
                this.sharedService.selfieResponse.gender = data.faces[0].attributes.gender.value;
                this.refressData();
              }else{
                this.errMessage = "There is some error please take another selfie or give data manuaLly";
                this.gotResponse = true;
                this.loader = false;
                $('#myModal').modal('hide');
              }
            },err => {
              console.dir(JSON.stringify(err));
              this.errMessage = "There is some technical issue Please try another again";
              this.loader = false;
              $('#myModal').modal('hide');
            },
            
          );
        // this.fileChange(event);
        //console.log(event.target.result);
      }
      
    }
}

refressData(){
  // call need to be made
  this.errMessage = "";
  this.loader = true;
  if(!this.sharedService.selfieResponse.income){
    this.sharedService.selfieResponse.income = "0";
  }
  this.http.get<Insurance[]>("http://localhost:8088/fetch/insurance/"+this.sharedService.selfieResponse.age+"/"+this.sharedService.selfieResponse.income)
    .subscribe(
      res => {
        console.log(res);
        this.sharedService.selfieResponse.insurances = res;
        this.showInsurance = true;
        this.gotResponse = true;
      },err => {
        console.dir(err);
        this.errMessage = "There was some technical error please try again";
      },
      () => {
        this.loader = false;
        $('#myModal').modal('hide');
      }
    )
}



}


