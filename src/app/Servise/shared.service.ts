import { Injectable } from '@angular/core';
import { SelfieResponse } from '../module/selfieResponse';

@Injectable()
export class SharedService {

  public selfieResponse: SelfieResponse = new SelfieResponse();

  constructor() { }

}
