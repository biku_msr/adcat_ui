import { Insurance } from "./insurance";


export class SelfieResponse{
    age: string;
    gender: string;
    name: string;
    income: string;
    insurances: Array<Insurance> = new Array<Insurance>();
}